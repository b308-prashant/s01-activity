import matplotlib.pyplot as plt
import pandas as pd
import numpy as np


movie= pd.read_csv('./movies_data.csv')

plt.figure(figsize=(7,4))

plt.title("Movies watched by Kim's family from 2008-2012")

plt.xlabel('Year')
plt.ylabel('Number of movies')
plt.xticks([2008, 2009,2010, 2011, 2012])
plt.yticks(range(6,21,2))


plt.plot(movie.Year, movie.Number_of_Movies, 'b.-', label='movies watched', markersize=15, markeredgecolor='blue')
plt.savefig('movies watched',dpi=100)
plt.legend()
plt.show()

cans=pd.read_csv('./data_cans.csv')
plt.figure(figsize=(8,4))
plt.xticks(range(2008,2014))
plt.yticks(range(200,950,100))
plt.title('Island vs Eden collection of Aluminium Beverage Cans')
plt.xlabel('Year')
plt.ylabel('weight of cans in pounds')


plt.plot(cans.Year, cans.Island_Middle_School, 'b.--', label='Island Middle School', markersize=12, markeredgecolor='red')
plt.plot(cans.Year, cans.Eden_Middle_Schhol, 'y.-', label='Eden Middle School', markersize=12, markeredgecolor='red')
plt.savefig('Aluminium cans', dpi=100)
plt.legend()
plt.show()

# plt.figure(figsize=(6,4))
x=[3,10,5,8]
y=['Ant','Lady_bug','Grasshopper', 'spider']
plt.xticks([1,2,3,4,5,6,7,8,9,10])
plt.title('Jasons insects')
plt.xlabel('total count')
plt.ylabel('insect type')
color_bar=['magenta', 'cyan','orange','pink']
plt.savefig('insect',dpi=100)
plt.barh(y,x,color=color_bar)
plt.show()

plt.figure(figsize=(5,8))
plt.title('Data A,B and C')
plt.xlabel('Data')
plt.ylabel('value')
median_props={'linewidth':3}
labels=['A','B','C']
A=[46, 54, 33, 68, 43, 39, 60
]
B=[58, 67, 44, 72, 51, 42, 60, 46, 69
]
C=[67, 100, 94, 77, 80, 62, 79, 68, 95, 86, 73, 84
]
boxes=plt.boxplot([A, B, C], labels=labels, patch_artist=True, medianprops=median_props)
for box in boxes['boxes']:
	box.set(color='#abcdef', linewidth=4)
	box.set(facecolor='#cecece'
		)
plt.savefig('BOx plot',dpi=100)	
plt.show()